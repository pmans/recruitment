
    <div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
        <ul class="nav menu">
            <li class="active"><a href="{{ route('home') }}"><span class="glyphicon glyphicon-dashboard"></span> DASBOARD</a></li>
            <li class="active ">
                <a href="{{ route('list') }}">
                    <span class="glyphicon glyphicon-th"></span> SEND LETTER  
                </a>
            </li>

            <li class="active ">
                <a href="{{ route('letters-view') }}">
                    <span class="glyphicon glyphicon-th"></span> LETTERS  
                </a>
            </li>
        </ul>

        <div class="attribution">Test Recruitment</a></div>
    </div>
