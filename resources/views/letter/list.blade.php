@extends('layouts.master')

@section('content')

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">           
    <div class="row">
        <div class="col-lg-12">
            <ol class="breadcrumb">
                <li><a href="#"><span class="glyphicon glyphicon-home"></span></a></li>
                <li class="active">Dashboard</li>
            </ol>
        </div>
    </div>

<div class="panel panel-default">
    <div class="panel-heading">LIST USER</div>
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-12">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Fullname</th>
                                <th>Email</th>
                                <th>Write Letter</th>   
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($users as $user)
                        @if ($user->username != Auth::user()->username)
                            <tr>
                                <td>{{ $user->fullname }}</td>
                                <td>{{ $user->email }}</td>
                                <td><a href="letter/{{ $user->id }}/send"><button type="button" class="btn btn-info"><span class="glyphicon glyphicon-envelope"></span> Write Letter </button></a></td>
                            </tr>
                        @endif
                        @endforeach   
                         </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

@endsection
