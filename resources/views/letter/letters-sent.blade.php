@extends('layouts.master')

@section('content')

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">           
    <div class="row">
        <div class="col-lg-12">
            <ol class="breadcrumb">
                <li><a href="#"><span class="glyphicon glyphicon-home"></span></a></li>
                <li class="active">Dashboard</li>
            </ol>
        </div>
    </div>

<div class="panel panel-default">
    <div class="panel-heading">
    <a href="{{ route('letters-view') }}"><button type="button" class="btn btn-info"><span class="glyphicon glyphicon-envelope"></span> Inbox </button></a>   <a href="{{ route('letters-sent') }}"><button type="button" class="btn btn-info"><span class="glyphicon glyphicon-envelope"></span> Sent Mail </button></a></div>

        <div class="panel-body">
            <div class="row">
                <div class="col-lg-12">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>To</th>
                                <th>Subject</th>
                                <th>Letter</th>   
                            </tr>
                        </thead>
                        <tbody>

                        @foreach ($users as $user)  
                        @if (Auth::user()->id == $user->id_sender)
                            <tr>
                                <td>{{ $user->to }}</td>
                                <td><span class="label label-success">{{ $user->subject }}</span></td>
                                <td>{{ $user->letter }}</td>
                                <td><a href="letter/{{ $user->id }}/delete"><button type="button" class="btn btn-danger">DELETE </button></a></td>
                            </tr>
                        @endif
                        @endforeach
                         </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

@endsection
