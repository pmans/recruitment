@extends('layouts.master')

@section('content')

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">           
    <div class="row">
        <div class="col-lg-12">
            <ol class="breadcrumb">
                <li><a href="#"><span class="glyphicon glyphicon-home"></span></a></li>
                <li class="active">Dashboard</li>
            </ol>
        </div>
    </div>
                

<div class="panel panel-default">
    <div class="panel-heading">SEND LETTER</div>
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel-body">
                        <div class="col-md-12">
                            <form action="/home/letter/send" method="POST" role="form" enctype="multipart/form-data">
                            {{csrf_field()}}
                            
                                <div class="form-group">
                                    <label>TO </label>
                                    <input name="to" class="form-control" value="{{ $user['email'] }}" readonly>
                                </div>

                                <div class="form-group">
                                    <label>SUBJECT</label>
                                    <input name="subject" class="form-control" placeholder="SUBJECT" >
                                </div>

                                <div class="form-group">
                                    <label>LETTER</label>
                                    <textarea name="letter" id="ckeditor"></textarea>
                                </div>

                                <div class="form-group">
                                    <input type="hidden" name="id_sender" class="form-control" value="{{ Auth::user()->id }}" >
                                </div>

                                <button type="submit" class="btn btn-info btn-fill btn-wd">SEND</button>
                                <button type="reset" class="btn btn-default">Reset Button</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

@endsection
