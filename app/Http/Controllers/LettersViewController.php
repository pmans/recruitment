<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Letter;
use App\User;

class LettersViewController extends Controller
{


    private $letters;

    public function __construct()
    {
        $this->letters = Letter::all();
    }


   public function index()
    {
     $letters = $this->letters;
     $users = User::join('letters', 'users.id','=','letters.id_sender')->select('users.id','users.email','letters.to','letters.id_sender','letters.subject','letters.letter','letters.id as letters')->get();
     return view('letter.letters-view', compact('letters','users'));
    }

    public function indexsent()
    {
     $letters = $this->letters;
     $users = User::join('letters', 'users.id','=','letters.id_sender')->select('users.id','users.email','letters.to','letters.id_sender','letters.subject','letters.letter','letters.id as letters')->get();
     return view('letter.letters-sent', compact('letters','users'));
    }

}
