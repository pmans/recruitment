<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Letter;
use App\Mail\SendMail;

class LetterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = \App\User::all(); 
        return view('letter.list', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(User $users)
    {  

         
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);     
        return view('letter.list', [
            'fullname' => $user->fullname,
            'username' => $user->username,
            'email' => $user->email,

            'id' => $user->id
        ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function send(User $user)
    {
        return view('letter.send', [
            'user' => $user
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function post()
    {
         Letter::create([
            'to' => request('to'),
            'subject' => request('subject'),
            'letter' => request('letter'),
            'id_sender' => request('id_sender')


        ]);

     return redirect('/home/letter/letters-sent');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
