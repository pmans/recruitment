<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($id)
    {
         $user = User::find($id);     
        return view('letter.list', [
            'fullname' => $user->fullname,
            'username' => $user->username,
            'email' => $user->email,

            'id' => $user->id
        ]);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(User $users)
    {
        return view('letter.send', [
            'user' => $user
            ]);
    }
}
