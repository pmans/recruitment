<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Letter extends Model
{
      protected $fillable = ['to', 'subject','letter','id_sender'];
}
