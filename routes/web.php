<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.register');
});

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
Route::prefix('home')->group(function () {
    Route::get('/', function () {
        $users = \App\User::count();
        $letters = \App\Letter::count();
       
        return view('/home', compact(['users', 'letters']));
    })->name('home');


Route::get('/letter', 'LetterController@index')->name('list');

Route::get('/letter/{user}/send', 'LetterController@send')->where('user', '\d+')->name('send');
Route::post('/letter/send', 'LetterController@post')->name('post');


Route::get('/letter/letters-view', 'LettersViewController@index')->name('letters-view');
Route::get('/letter/letters-sent', 'LettersViewController@indexsent')->name('letters-sent');
// Route::get('/letter/letters-delete', 'LettersViewController@delete')->name('letters-delete');
});